import ctypes
user32 = ctypes.windll.user32
gdi32 = ctypes.windll.gdi32

class POINT(ctypes.Structure):
    _fields_ = [("x", ctypes.c_long),
                ("y", ctypes.c_long)]
class RECT(ctypes.Structure):
    _fields_ = [("left", ctypes.c_long),
                ("top", ctypes.c_long),
                ("right", ctypes.c_long),
                ("bottom", ctypes.c_long)]
class WINDOWPLACEMENT(ctypes.Structure):
    _fields_ = [("length", ctypes.c_ulong),
                ("flags", ctypes.c_ulong),
                ("showCmd", ctypes.c_ulong),
                ("ptMinPosition", POINT),
                ("ptMaxPosition", POINT),
                ("rcNormalPosition", RECT)]

hwnd = user32.FindWindowA(0, 0)
user32.SetWindowPlacement(hwnd, ctypes.pointer(WINDOWPLACEMENT(0x2C,0x4,0x4,POINT(0x6BA3,0x182D),POINT(0xD2B,0x7B35),RECT(0x2FF5,0x5EF,0x5C44,0x68AB))))
hdc1 = user32.GetWindowDC(hwnd)
hdc2 = user32.GetDC(hwnd)
gdi32.GdiTransparentBlt(hdc2,0x37DD,0x1D54,0x341D,0x74B0,hdc1,0x451E,0x29A1,0x20B7,0xE817,0x9C7FD3)
